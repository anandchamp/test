<?php

	class string_lib{

		public function uc_check($str){
			
			if(is_array($str)):
				//array isn't allowed for string match
				return 0;
			else:
				return preg_match('/^[A-Z]/', $str);	
			endif;
			
		}

	}

	//create a object string_lib class
	$object = new string_lib;
	//call class function with string to check
	if($object->uc_check('Hello')):
		echo "First letter is uppercase";
	else:
		echo "First letter isn't uppercase";
	endif;
	
?>